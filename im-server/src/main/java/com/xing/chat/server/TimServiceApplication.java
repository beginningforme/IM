package com.xing.chat.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@Slf4j
@SpringBootApplication
public class TimServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(TimServiceApplication.class,args);
        log.info("Start tim server success!!!");
    }
}

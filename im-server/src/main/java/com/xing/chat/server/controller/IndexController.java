package com.xing.chat.server.controller;

import com.xing.chat.common.res.BaseResponse;
import com.xing.chat.server.api.ServerApi;
import com.xing.chat.server.api.req.SendMsgReq;
import com.xing.chat.server.api.res.SendMsgRes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class IndexController implements ServerApi {


    @Override
    @RequestMapping(value = "sendMsg", method = RequestMethod.POST)
    @ResponseBody
    public BaseResponse<SendMsgRes> sendMsg(SendMsgReq sendMsgReq) {


        return null;
    }

}

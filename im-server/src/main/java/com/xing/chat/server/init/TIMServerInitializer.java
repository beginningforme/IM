package com.xing.chat.server.init;

import com.xing.chat.server.handler.TIMServerHandle;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.IdleStateHandler;


public class TIMServerInitializer extends ChannelInitializer<Channel> {

    private final TIMServerHandle timServerHandle = new TIMServerHandle();


    @Override
    protected void initChannel(Channel channel) throws Exception {
        channel.pipeline()
                // 20秒没有收到客户端发送消息或心跳就触发读空闲，执行TIMServerHandle的userEventTriggered方法关闭客户端连接
                .addLast(new IdleStateHandler(20,0,0))

                .addLast(timServerHandle);
    }
}

package com.xing.chat.server.handler;

import com.xing.chat.common.constant.Constants;
import com.xing.chat.common.protocol.TIMReqMsg;
import com.xing.chat.common.utils.NettyAttrUtil;
import com.xing.chat.server.util.SessionSocketHolder;
import com.xing.chat.server.util.SpringBeanFactory;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@ChannelHandler.Sharable
public class TIMServerHandle extends SimpleChannelInboundHandler<TIMReqMsg> {


    /**
     * 取消绑定
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }



    /**
     * 心跳检查时触发该方法
     * @param ctx
     * @param evt
     * @throws Exception
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

        if(evt instanceof IdleStateEvent){
            IdleStateEvent event = (IdleStateEvent) evt;
            //如果是读时间
            if(event.state() == IdleState.READER_IDLE){
                log.info("定时检测客户端是否存活");
            }
        }
        super.userEventTriggered(ctx, evt);
    }

    /**
     *
     * @param channelHandlerContext
     * @param msg
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, TIMReqMsg msg) throws Exception {
        log.info("received msg=[{}]", msg.toString());

        /**
         * 如果是登陆
         */
        if(msg.getType() == Constants.CommandType.LOGIN){
            //保存channel连接
            SessionSocketHolder.put(msg.getRequestId(), (NioSocketChannel) channelHandlerContext.channel());
            //保存登陆信息
            SessionSocketHolder.saveSession(msg.getRequestId(),msg.getReqMsg());
            log.info("client [{}] online success!!", msg.getReqMsg());

        }


        /**
         * 心跳消息
         */
        if(msg.getType() == Constants.CommandType.PING){
            //设置读取的时间
            NettyAttrUtil.updateReaderTime(channelHandlerContext.channel(),System.currentTimeMillis());
            //向客户端响应 pong 消息
            TIMReqMsg timReqMsg = SpringBeanFactory.getBean("heartBeat",TIMReqMsg.class);
            channelHandlerContext.writeAndFlush(timReqMsg).addListeners((ChannelFutureListener)future -> {
                if(!future.isSuccess()){
                    log.error("IO error,close Channel");
                    future.channel().close();
                }
            });
        }

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }
}

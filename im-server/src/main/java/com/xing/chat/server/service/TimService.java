package com.xing.chat.server.service;

import com.xing.chat.server.init.TIMServerInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.InetSocketAddress;

/**
 * 消息发送
 */
@Service
@Slf4j
public class TimService {


    EventLoopGroup workerGroup = new NioEventLoopGroup();
    EventLoopGroup bossGroup = new NioEventLoopGroup();


    @Value("${tim.server.port}")
    private int port;

    /**
     * 启动IM服务端
     */
    @PostConstruct
    public void start() throws InterruptedException {
        //定义服务端启动类
        ServerBootstrap bootstrap = new ServerBootstrap();

        //使用NioServerSocketChannel  指定Nio的模式，如果是客户端就是NioSocketChannel
        bootstrap.channel(NioServerSocketChannel.class);
        //设置线程组
        bootstrap.group(bossGroup,workerGroup)
                 //绑定端口
                .localAddress(new InetSocketAddress(port))
                //保证长链接
                .childOption(ChannelOption.SO_KEEPALIVE,true)
                //为每个连接添加NettyServerHandler 处理器
                .childHandler(new TIMServerInitializer());

        //绑定端口并进行阻塞
        ChannelFuture future = bootstrap.bind().sync();
        if(future.isSuccess()){
            log.info("Start tim server success!!!");
        }

        //此处不用关闭阻塞，Springboot服务启动，main线程已经被阻塞
//        // 关闭连接之前一直阻塞
//        future.channel().closeFuture().sync();
//
//        System.out.println("connect结束");

    }


    /**
     * 服务关闭
     */
    @PreDestroy
    public void destory(){
        workerGroup.shutdownGracefully().syncUninterruptibly();
        bossGroup.shutdownGracefully().syncUninterruptibly();
        log.info("Close tim server success!!!");
    }

}

package com.xing.chat.server.util;

import io.netty.channel.socket.nio.NioSocketChannel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 记录客户端的连接、以及登陆信息
 */
public class SessionSocketHolder {

    /**
     *  保存请求连接
     */
    private static final Map<Long, NioSocketChannel> CHANNEL_MAP = new ConcurrentHashMap<>();

    /**
     * 保存登陆信息
     */
    private static final Map<Long,String> SESSION_MAP = new ConcurrentHashMap<>();


    /**
     * 新增channel连接
     *
     * @param userId
     * @param socketChannel
     */
    public static void put(Long userId, NioSocketChannel socketChannel) {
        CHANNEL_MAP.put(userId, socketChannel);
    }


    /**
     * 获取连接
     *
     * @param userId
     * @return
     */
    public static NioSocketChannel get(Long userId) {
        return CHANNEL_MAP.get(userId);
    }

    /**
     * 删除连接
     *
     * @param socketChannel
     */
    public static void remove(NioSocketChannel socketChannel) {
        CHANNEL_MAP.entrySet().removeIf(entry -> entry.getValue() == socketChannel);
    }


    /**
     * 保存登陆信息
     * @param userId
     * @param userName
     */
    public static void saveSession(Long userId, String userName) {
        SESSION_MAP.put(userId, userName);
    }

    /**
     * 保存登陆信息
     * @param id
     */
    public static void removeSession(Long id) {
        SESSION_MAP.keySet().removeIf(key-> key.intValue() == id.intValue());
    }




}


package com.xing.chat.server.config;

import com.xing.chat.common.constant.Constants;
import com.xing.chat.common.protocol.TIMReqMsg;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {



    /**
     * 构建心跳对象
     * @return
     */
    @Bean("heartBeat")
    public TIMReqMsg heartBeat(){
        return new TIMReqMsg(0L, "pong", Constants.CommandType.PING);
    }
}

package com.xing.chat.common.req;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseRequest implements Serializable {

    /**
     * 唯一的请求Id
     */
    private String reqNo;

    /**
     * 当前请求的时间戳
     */
    private long timeStamp;
}

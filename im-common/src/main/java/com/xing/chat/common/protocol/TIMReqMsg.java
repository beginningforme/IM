package com.xing.chat.common.protocol;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TIMReqMsg {

    /**
     * 请求id
     */
    private Long requestId;

    /**
     * 请求消息
     */
    private String reqMsg;

    /**
     * 消息事件类型
     */
    private Integer type;
}

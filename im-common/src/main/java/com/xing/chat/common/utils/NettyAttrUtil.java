package com.xing.chat.common.utils;

import io.netty.channel.Channel;
import io.netty.util.AttributeKey;
import org.apache.commons.lang3.StringUtils;

/**
 * 设置读时间
 */
public class NettyAttrUtil {

    private static final AttributeKey<String> ATTR_KEY_READER_TIME = AttributeKey.valueOf("readerTime");



    public static void updateReaderTime(Channel channel, Long time){
        channel.attr(ATTR_KEY_READER_TIME).set(time.toString());
    }


    public static Long getReaderTime(Channel channel){
        String value = channel.attr(ATTR_KEY_READER_TIME).get();
        if(StringUtils.isNotBlank(value)){
           return Long.parseLong(value);
        }
        return null;
    }

}

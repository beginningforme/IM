package com.xing.chat.common.res;

import lombok.Data;

import java.io.Serializable;

@Data
public class BaseResponse <T> implements Serializable {

    /**
     * 错误吗
     */
    private String resultCode;

    /**
     * 错误原因
     */
    private String resultMsg;

    /**
     * 返回数据
     */
    private T data;
}

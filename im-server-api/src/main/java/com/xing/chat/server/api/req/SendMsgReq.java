package com.xing.chat.server.api.req;


import com.xing.chat.common.req.BaseRequest;
import lombok.Data;

@Data
public class SendMsgReq extends BaseRequest {

    /**
     * 发送的消息
     */
    private String msg;

    /**
     * 用户id
     */
    private String userId;

}

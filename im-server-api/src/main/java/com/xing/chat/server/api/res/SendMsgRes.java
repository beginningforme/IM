package com.xing.chat.server.api.res;

import lombok.Data;

/**
 *
 */
@Data
public class SendMsgRes {

    /**
     * 发送的消息
     */
    private String msg;
}

package com.xing.chat.server.api;

import com.xing.chat.server.api.req.SendMsgReq;
import com.xing.chat.server.api.res.SendMsgRes;

/**
 * Netty服务端API
 */
public interface ServerApi {


    /**
     * 发送消息
     * @param sendMsgReq
     * @return
     */
    Object sendMsg(SendMsgReq sendMsgReq);
}
